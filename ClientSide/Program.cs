﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSide
{
    class Program
    {
        static void Main(string[] args)
        {
            RemotingConfiguration.Configure("Client.exe.config");

            RemoteClass obj = new RemoteClass();



            if (obj.Equals(null))

            {

                System.Console.WriteLine("Error: unable to locate server");

            }

            else

            {

                String strArgs;

                if (args.Length == 0)

                {

                    strArgs = "Client";

                }

                else

                {

                    strArgs = args[0];

                }

                obj.WriteName(strArgs);

                System.Console.WriteLine(obj.ReadWelcome());

                System.Console.ReadLine();

            }
        }
    }
}
