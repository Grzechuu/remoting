﻿using System;
using ConsoleApp2.Remote;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;



namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            TcpChannel channel = new TcpChannel();

            ChannelServices.RegisterChannel(channel);

            RemotingConfiguration.RegisterActivatedClientType(typeof(RemoteClass), "tcp://localhost:8888/remote");
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            obj.insert("Test1", "192.168.1.1", 8888, 1);
            Application.Run(new Form1());

        }
       
    }
}
