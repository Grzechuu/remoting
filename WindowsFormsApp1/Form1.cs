﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using ConsoleApp2.Remote;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public string hostNameTTL;
        public string hostName;
        public string name;
        public int port;
        public int index;
        public int hoops;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            // NAME
            name = textBox5.Text;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //HostName
            hostName = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //Name
            name = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //port
            try
            {
                port = Int32.Parse(textBox3.Text);
            }
            catch
            {
                port = 1;
            }
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            try
            {
                index = Int32.Parse(textBox4.Text);
            }
            catch
            {
                index = 1;
            }
        }
        public void button1_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "Names";
            dataGridView1.Columns[1].Name = "Port";
            dataGridView1.Columns[2].Name = "HostNames";
            dataGridView1.Columns[3].Name = "Przeskoki";
            dataGridView1.Columns[4].Name = "TTL";


            var list = obj.remotingList;
          
            foreach (var item in list)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value =item._name;
                row.Cells[1].Value = item._port;
                row.Cells[2].Value = item._hostName;
                row.Cells[3].Value = item._hoop;
                row.Cells[4].Value = item._ttl;
                dataGridView1.Rows.Add(row);
            }
            //var a = obj.returnNamesValues();
            //var c = obj.returnHostNamesValues();
            //var b = obj.returnPortValues();
            //var d = obj.returnHoopValues();
            //var f = obj.returnTTLValues();
            //for(int i = 0; i < 100; i++)
            //{
            //    if(i == 0)
            //    {
                 
            //    }
            //    if(a[i] != null && c[i] != null)
            //    {
                    
            //        if(a[i] != null)
            //        {
            //            row.Cells[0].Value = a[i];
            //            row.Cells[1].Value = b[i];
            //            row.Cells[2].Value = c[i];
            //            row.Cells[3].Value = d[i];
            //            row.Cells[4].Value = f[i];
            //            dataGridView1.Rows.Add(row);
            //        }
            //    }
            //}
            
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");

            var a = obj.insert(name, hostName, port, hoops);
            if (a == 0)
            {
                MessageBox.Show("Taki host juz istnieje");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            var a = obj.getPort(index);
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "Names";
            dataGridView1.Columns[1].Name = "Port";
            dataGridView1.Columns[2].Name = "HostNames";
            dataGridView1.Columns[3].Name = "Przeskoki";
            dataGridView1.Columns[4].Name = "TTL";
            foreach (var item in a)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value = item._name;
                row.Cells[1].Value = item._port;
                row.Cells[2].Value = item._hostName;
                row.Cells[3].Value = item._hoop;
                row.Cells[4].Value = item._ttl;
                dataGridView1.Rows.Add(row);

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            var a = obj.getHostName(hostName);
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "Names";
            dataGridView1.Columns[1].Name = "Port";
            dataGridView1.Columns[2].Name = "HostNames";
            dataGridView1.Columns[3].Name = "Przeskoki";
            dataGridView1.Columns[4].Name = "TTL";
            foreach (var item in a)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value = item._name;
                row.Cells[1].Value = item._port;
                row.Cells[2].Value = item._hostName;
                row.Cells[3].Value = item._hoop;
                row.Cells[4].Value = item._ttl;
                dataGridView1.Rows.Add(row);

            };
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            var a = obj.search(name);
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "Names";
            dataGridView1.Columns[1].Name = "Port";
            dataGridView1.Columns[2].Name = "HostNames";
            dataGridView1.Columns[3].Name = "Przeskoki";
            dataGridView1.Columns[4].Name = "TTL";
            foreach (var item in a)
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value = item._name;
                row.Cells[1].Value = item._port;
                row.Cells[2].Value = item._hostName;
                row.Cells[3].Value = item._hoop;
                row.Cells[4].Value = item._ttl;
                dataGridView1.Rows.Add(row);

            };

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            try
            {
                hoops = Int32.Parse(textBox6.Text);
            }
            catch
            {
                hoops = 1;
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            hostNameTTL = textBox7.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
            obj.reduceTTL(hostNameTTL);
        }
    }
}
