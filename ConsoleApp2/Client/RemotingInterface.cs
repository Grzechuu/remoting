﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Remote
{
    interface RemotingInterface
    {
        void WriteName(string str);
        string ReadWelcome();
    }
}
