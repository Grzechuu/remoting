﻿using ConsoleApp2.Remote;
using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;


public class Client
{
    public static void Main(string[] args)

    {

        TcpChannel chan = new TcpChannel();

        ChannelServices.RegisterChannel(chan);



        // Create an instance of the remote object
        RemotingConfiguration.RegisterActivatedClientType(typeof(RemoteClass), "tcp://localhost:8888/remote");
        RemoteClass obj = (RemoteClass)Activator.GetObject(typeof(RemoteClass), "tcp://localhost:8888/remote");
        
        if (obj.Equals(null))
        {
            Console.WriteLine("Error: unable to locate server");
        }
        else
        {
            String strArgs;

            if (args.Length == 0)
            {
                strArgs = "Client";
            }
            else

            {
                strArgs = args[0];

            }
     
        }
        obj.insert("Test1", "192.168.1.1", 8888);
        Console.ReadKey();

    }
}