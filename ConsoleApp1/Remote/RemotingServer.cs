﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Diagnostics;
using Microsoft.Win32;

namespace ConsoleApp2.Remote
{
    public class RemotingServer
    {
        public static void Main(string[] args)

        {

            TcpChannel channel = new TcpChannel(8888);

            ChannelServices.RegisterChannel(channel);


            RemotingConfiguration.RegisterWellKnownServiceType(typeof(RemoteClass), "remote" ,WellKnownObjectMode.Singleton);

            Console.WriteLine("The Server is ready .... Press the enter key to exit...");
       
            Console.ReadLine();

        }
    }
}
