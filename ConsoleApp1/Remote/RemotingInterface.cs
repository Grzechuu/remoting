﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Remote
{
    public interface RemotingInterface
    {
      

        int search(string s);
        int insert(string s, string hostName, int portNumber, int hoops);
        int getTTL(int index);
        int getHoop(int index);
        string getHostName(int index);
        void WriteName(string str);
        string ReadWelcome();
    }
}
