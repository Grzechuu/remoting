﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Remote
{
    [Serializable]
    public class RemotingObject
    {
        public string _name;
        public string _hostName;
        public int _port;
        public int _hoop;
        public int _ttl;


        public RemotingObject(string name, string hostName, int port, int hoop, int ttl)
        {
            _name = name;
            _hostName = hostName;
            _port = port;
            _hoop = hoop;
            _ttl = ttl;
        }

        public string returnNameValue()
        {
            return _name;
        }
        public string returnHostNameValue()
        {
            return _hostName;
        }
        public int returnPortValue()
        {
            return _port;
        }
        public int returnHoopValue()
        {
            return _hoop;
        }
        public int ReturnTTLValue()
        {
            return _ttl;
        }
    }
}
