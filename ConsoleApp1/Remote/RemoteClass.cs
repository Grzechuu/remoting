﻿using ConsoleApp1.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Remote
{
    public class RemoteClass : MarshalByRefObject
    {
        public List<RemotingObject> remotingList = new List<RemotingObject>();
        public static string strWelcomeClient;
        public static int maxSize = 100;
        public int[] ttl = new int[maxSize];
        public int[] hoop = new int[maxSize];
        private string[] names = new string[maxSize];
        private string[] hosts = new string[maxSize];
        private int[] ports = new int[maxSize];
        private int dirSize = 0;

        public RemoteClass()
        {
            Console.WriteLine("Object created");
        }

        public List<RemotingObject> search(string s)
        {
            return remotingList.Select(x => x).Where(y => y._name == s).ToList();
        }

        public int insert(string s, string hostName, int portNumber, int hoops)
        {
            var exist = remotingList.Exists(x => x._hostName == hostName);
            if (exist)
            {
                return 0;
            }
            var remote = new RemotingObject(s, hostName, portNumber, hoops, 128);
            remotingList.Add(remote);
            return 1;
        }
        public void reduceTTL(string hostname)
        {
            var element = remotingList.FirstOrDefault(x => x._hostName == hostname);
            var result = element._ttl - element._hoop;
            if(result > 0)
            {
                element._ttl = result;
            }
            else
            {
                remotingList.Remove(element);
            }
            
        }
     
        public List<RemotingObject> getPort(int port)
        {
            return remotingList.Select(x => x).Where(y => y._port == port).ToList();
        }

        public int getTTL(int index)
        {
            return ttl[index];
        }
        public int getHoop(int index)
        {
            return hoop[index];
        }

        public List<RemotingObject> getHostName(string hostname)
        {
            return remotingList.Select(x => x).Where(y => y._hostName == hostname).ToList();
        }



        public string ReadWelcome()

        {

            return strWelcomeClient;

        }

        public void WriteName(string strNameFromClient)

        {

            strWelcomeClient = "HI " + strNameFromClient + ". Welcome to Remoting World!!";
        }

    }
}
